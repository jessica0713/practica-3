package mx.unitec.practica3

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_picker_time.*
import mx.unitec.practica3.ui.TimePickerFragment

class PickerTimeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_time)
    }
    @SuppressLint("SetTextI18n")
    fun showDatePikerDialo(v: View) {
    //val timePickerFragment = TimePickerFragment()

    val timePickerFragment = TimePickerFragment.newInstance(TimePickerDialog.OnTimeSetListener
    { view, hourOfDay, minute ->
        pkrTime.setText("${hourOfDay}:${minute}")
    })
        timePickerFragment.show(supportFragmentManager, "timePicker")
    }

}
//Se agrega PickerDateActivity//